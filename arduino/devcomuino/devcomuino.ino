#include <EEPROM.h>

#include "Wire.h"
#include "devcomuino.h"

const unsigned int MAXARGS = 5;
const unsigned int MAXBYTES = 16;
const unsigned int MAXCMDLENGTH = 64;
const unsigned int MAXARGLENGTH = 16;

// Command parsing
size_t cmdptr = 0;
char command[MAXCMDLENGTH];
unsigned int argc = 0;
char argv[MAXARGS][MAXARGLENGTH];

// Parse command execute the right function
void runcommand() {
    // Tokenize string into command and argument
    char *tok = strtok(command, " ");
    argc = 0;
    while (tok != NULL) {
        strcpy(argv[argc++], tok);
        tok = strtok(NULL, " ");
    }

    // Execute the right command
    if (strncmp("HELP", argv[0], 4) == 0) {  // Print help menu
        cmdHELP();
    }
    // need to check ADC functionality
    else if (strncmp("ADC", argv[0], 3) == 0) {  // Read ADC
        if (argc != 2) {
            Serial.println("ERR wrong number of args to ADC");
            return;
        }
        cmdADC(atoi(argv[1]));
    }

    else if (strncmp("EEPROM", argv[0], 6) == 0) {  // EEPROM commands

        if (strncmp("WRITE", argv[1], 5) == 0) {
            if (argc < 4) {
                Serial.println("ERR wrong number of args to EEPROM WRITE");
                return;
            }

            int address = atoi(argv[2]);
            int value = atoi(argv[3]);
            cmdEEPROMwrite(address, value);
        }

        if (strncmp("READ", argv[1], 4) == 0) {
            if (argc < 3) {
                Serial.println("ERR wrong number of args to EEPROM READ");
                return;
            }
            int address = atoi(argv[2]);
            cmdEEPROMread(address);
        }

    } else if (strncmp("I2C", argv[0], 3) == 0) {  // I2C commands
        if (argc < 4) {
            Serial.println("ERR wrong number of args to I2C");
            return;
        }

        int addr = 0;
        sscanf(argv[2], "%02x", &addr);
        if (strncmp("WRITE", argv[1], 5) == 0) {
            cmdI2Cwrite(addr, argv[3]);
        } else if (strncmp("READ", argv[1], 4) == 0) {
            cmdI2Cread(addr, atoi(argv[3]));
        } else {
            Serial.println("ERR unknown I2C command");
        }

    } else if (strncmp("DGT", argv[0], 3) == 0) {  // DGT commands
        if (argc == 3) {
            int chan = atoi(argv[2]);
            if (strncmp("OUT", argv[1], 3) == 0) {
                cmdDGToutput(chan);
            } else if (strncmp("IN", argv[1], 2) == 0) {
                cmdDGTinput(chan);
            } else if (strncmp("PULLUP", argv[1], 6) == 0) {
                cmdDGTinputpullup(chan);
            } else if (strncmp("READ", argv[1], 4) == 0) {
                cmdDGTread(chan);
            } else {
                Serial.println("ERR unknown DGT command");
            }
        } else if (argc == 4 && strncmp("WRITE", argv[1], 5) == 0) {
            int chan = atoi(argv[2]);
            int value = atoi(argv[3]);
            cmdDGTwrite(chan, value);
        } else {
            Serial.println("ERR wrong number of args to DGT");
            return;
        }
    } else {
        Serial.println("ERR unknown command");
    }
}

//
// The commands
//

//
// Print
void cmdHELP() {
    Serial.println(F("Hello World from DevComuino!"));
    Serial.println("");
    Serial.println(F("Commands:"));
    Serial.println(F("\tHELP - This help"));
    Serial.println(F("\tADC ch - Read ADC channel ch"));
    Serial.println(
        F("\tI2C WRITE addr byte-string - Write byte-string to I2C addr, "
          "MSB first"));
    Serial.println(F("\tI2C READ addr nbytes - Read nbytes from I2C addr"));
    Serial.println(F("\tEEPROM WRITE addr value - Write to addr in EEPROM"));
    Serial.println(F("\tEEPROM READ addr - Read addr from EEPROM"));
    Serial.println(F("\tDGT OUT ch - Set digital pin ch as output"));
    Serial.println(F("\tDGT IN ch - Set digital pin ch as input"));
    Serial.println(F("\tDGT PULLUP ch - Set digital pin ch as input w/pullup"));
    Serial.println(F("\tDGT READ ch - Read channel ch"));
    Serial.println(
        F("\tDGT WRITE ch 0|1 - Set channel ch to LOW (0) or HIGH (0)"));
}

//
// Read an analogue pin
void cmdADC(int channel) {
    float V;

    if (channel < sizeof(analog_pins)) {
        V = analogRead(analog_pins[channel]);
        Serial.println(V);
    } else {
        Serial.println("ERR invalid channel");
    }
}

//
// EEPROM Write
void cmdEEPROMwrite(int address, int value) { EEPROM.write(address, value); }

//
// EEPROM Read
void cmdEEPROMread(int address) {
    int val = EEPROM.read(address);
    Serial.println(val);
}

//
// I2C write
void cmdI2Cwrite(int address, char *cmd) {
    Wire.beginTransmission(address);

    int c = 0;
    for (int i = 0; i < strlen(cmd); i += 2) {
        sscanf(&cmd[i], "%02x", &c);
        Wire.write(c);
    }
    Wire.endTransmission();
    Serial.println("OK");
}

//
// I2C read
void cmdI2Cread(int address, unsigned int nBytes) {
    Wire.requestFrom(address, nBytes);

    unsigned char c;
    char cstr[4];

    // sprintf(cstr, "%02x:", nBytes);
    // Serial.print(cstr);

    for (unsigned int i = 0; i < nBytes; i++) {
        if (Wire.available()) {
            c = Wire.read();
            sprintf(cstr, "%02x", c);
            Serial.print(cstr);
            // Serial.print(strlen(cstr));
        } else {
            Serial.print("ERR");
        }
    }
    Serial.println();
}

//
// DGT input
void cmdDGTinput(int channel) {
    if (channel < n_digital_pins) {
        pinMode(channel, INPUT);
        Serial.println("OK");
    } else {
        Serial.println("ERR invalid channel");
    }
}

//
// DGT input with pullup resistors
void cmdDGTinputpullup(int channel) {
    if (channel < n_digital_pins) {
        pinMode(channel, INPUT_PULLUP);
        Serial.println("OK");
    } else {
        Serial.println("ERR invalid channel");
    }
}

//
// DGT output
void cmdDGToutput(int channel) {
    if (channel < n_digital_pins) {
        pinMode(channel, OUTPUT);
        Serial.println("OK");
    } else {
        Serial.println("ERR invalid channel");
    }
}

//
// DGT write
void cmdDGTwrite(int channel, int value) {
    if (channel < n_digital_pins) {
        if (value == 0) {
            digitalWrite(channel, LOW);
        } else {
            digitalWrite(channel, HIGH);
        }
        Serial.println("OK");
    } else {
        Serial.println("ERR invalid channel");
    }
}

//
// DGT read
void cmdDGTread(int channel) {
    uint8_t data = digitalRead(channel);
    Serial.println(data);
}

//
// The big main loop
//

//
// Setup serial
void setup() {
    Serial.begin(115200);
    Wire.begin();
}

//
// The main loop looks for commands
void loop() {
    if (Serial.available() == 0) return;

    // Read new data
    size_t length = Serial.readBytes(
        &command[cmdptr], min(Serial.available(), MAXCMDLENGTH - cmdptr));
    if (length == 0) return;  // No new data...
    cmdptr += length;

    // Check if command finished (new line)
    if (cmdptr < 2) return;
    if (command[cmdptr - 2] != '\r' && command[cmdptr - 1] != '\n') {
        if (cmdptr >= MAXCMDLENGTH - 1) {
            // overflow command. Clean-up buffer to avoid stalled program
            cmdptr = 0;
            Serial.print("ERR command too long");
        }
        return;
    }

    // There is a command! Process it...
    char *c = command;
    while (*c) {
        *c = toupper(*c);
        c++;
    }
    command[cmdptr - 2] = '\0';

    runcommand();
    cmdptr = 0;  // Reset command
}
