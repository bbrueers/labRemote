#include "RS_NGP804.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RS_NGP804)

RS_NGP804::RS_NGP804(const std::string& name) : SCPIPs(name, {"NGP804"}, 4) {}
