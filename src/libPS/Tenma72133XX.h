#ifndef TENMA72133XX_H
#define TENMA72133XX_H

#include <bits/stdc++.h>

#include <chrono>
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>

#include "IPowerSupply.h"

/** \brief Tenma 72-13300 series wide range programmable single channel DC power
 * supply
 *
 * Implementation for the Tenma 72-13350, 72-13360 single channel power
 * supplies.
 *
 * [Manual](http://www.farnell.com/datasheets/2819975.pdf)
 */
class Tenma72133XX : public IPowerSupply {
 public:
    Tenma72133XX(const std::string& name);
    ~Tenma72133XX() = default;

    /** \name Communication
     * @{
     */

    virtual bool ping();

    virtual std::string identify();

    /** @} */

    /** \name Power Supply Control
     * @{
     */

    virtual void reset();
    virtual void turnOn(unsigned channel = 1);
    virtual void turnOff(unsigned channel = 1);
    virtual bool isOn(unsigned channel = 1);
    //! if OCP is on
    virtual bool isOCP(unsigned channel = 1);
    //! if OVP is on
    virtual bool isOVP(unsigned channel = 1);
    //! show available status
    virtual std::bitset<8> getStatus(unsigned channel = 1);
    virtual void beepOn();
    virtual void beepOff();
    /** Save the panel setting in memory slot, only works on the active slot
     * \param Memory slot number (1 to 5)
     */
    virtual void saveSetting(unsigned mem);
    /** Recall saved setting in memory slot
     * \param Memory slot number (1 to 5)
     */
    virtual void recallSetting(unsigned mem);
    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    virtual void setCurrentLevel(double cur, unsigned channel = 1);
    virtual double getCurrentLevel(unsigned channel = 1);
    virtual void setCurrentProtect(double maxcur, unsigned channel = 1);
    virtual double getCurrentProtect(unsigned channel = 1);
    //! Switch OverCurrentProtection on
    virtual void OCPOn(unsigned channel = 1);
    //! Switch OverCurrentProtection off
    virtual void OCPOff(unsigned channel = 1);
    //! Set the output current slope in A/s, max 990000
    virtual void setCurrentSlope(double slope, unsigned channel = 1);
    virtual double getCurrentSlope(unsigned channel = 1);
    virtual double measureCurrent(unsigned channel = 1);
    //! set current priority
    virtual void setCurrentPriority(unsigned channel = 1);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    virtual void setVoltageLevel(double volt, unsigned channel = 1);
    virtual double getVoltageLevel(unsigned channel = 1);
    virtual void setVoltageProtect(double maxvolt, unsigned channel = 1);
    virtual double getVoltageProtect(unsigned channel = 1);
    //! Switch OverVoltageProtection on
    virtual void OVPOn(unsigned channel = 1);
    //! Switch OverVoltageProtection off
    virtual void OVPOff(unsigned channel = 1);
    //! Set the output voltage slope in V/s, max 990000
    virtual void setVoltageSlope(double slope, unsigned channel = 1);
    virtual double getVoltageSlope(unsigned channel = 1);
    virtual double measureVoltage(unsigned channel = 1);
    //! set voltage priority
    virtual void setVoltagePriority(unsigned channel = 1);
    /** @} */

 private:
    /**
     * Convert number to scientific notation
     *
     * \param a_value number to convert
     * \param n number of digitcs after the decimal
     *
     * \return x.xxxxxxEyy
     */
    template <typename T>
    std::string to_string_with_precision(const T a_value, const int n = 3) {
        std::ostringstream out;
        out << std::setprecision(n) << a_value;
        return out.str();
    }
};

#endif
