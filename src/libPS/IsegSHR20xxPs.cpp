#include "IsegSHR20xxPs.h"

#include "StringUtils.h"
#include "TextSerialCom.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(IsegSHR20xxPs)

IsegSHR20xxPs::IsegSHR20xxPs(const std::string& name)
    : IsegPs(name, {"SR020020"}, 2) {}