#ifndef ISEGSHR20XXPS_H
#define ISEGSHR20XXPS_H

#include <string>

#include "IsegPs.h"

/**
 * Implementation for the [ISEG SHR 20XX Dual Channel Output HV Power
 * Supplies](https://iseg-hv.com/en/products/detail/SHR).
 * The dual-channel ISEG power supplies appear to support the default [ISEG SHR
 * programming
 * model](https://iseg-hv.com/download/SOFTWARE/isegSCPI/SCPI_Programmers_Guide_en.pdf),
 * but this has not been checked.
 */
class IsegSHR20xxPs : public IsegPs {
 public:
    IsegSHR20xxPs(const std::string& name);
    ~IsegSHR20xxPs() = default;
};

#endif  // ISEGSHR20XXPS_H
