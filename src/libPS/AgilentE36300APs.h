#ifndef AGILENTE36300APS_H
#define AGILENTE36300APS_H

#include <string>

#include "AgilentPs.h"

/**
 * Implementation for the Agilent E36300A Triple Output
 * DC Power Supply ([Programming
 * Manual](https://literature.cdn.keysight.com/litweb/pdf/E36311-90008.pdf)).
 */
class AgilentE36300APs : public AgilentPs {
 public:
    AgilentE36300APs(const std::string& name);
    ~AgilentE36300APs() = default;
};

#endif  // AGILENTE36300APS_H
