#include <getopt.h>
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

#include "EquipConf.h"
#include "IMeter.h"
#include "Logger.h"
using json = nlohmann::json;

void usage(char* argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] command [parameters]"
              << std::endl;
    std::cerr << "List of possible COMMAND:" << std::endl;
    std::cerr << "  meas-voltage           Get reading of DC voltage [V]"
              << std::endl;
    std::cerr << "  meas-current           Get reading of DC current [A]"
              << std::endl;
    std::cerr << "  meas-res [use4w]       Get reading of resistance [Ohm] "
                 "(optionally perform a 4-wire resistance measurement by "
                 "providing the \"use4w\" parameter)"
              << std::endl;
    std::cerr << "  meas-cap               Get reading of capacitance [F]"
              << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -e, --equip   Config    Path to JSON equipment configuration file"
        << std::endl;
    std::cerr << " -n, --name    Name      Name of meter object (type: string)"
              << std::endl;
    std::cerr << " -c, --channel Ch        Set the channel to read from (type: "
                 "integer, default: 0)"
              << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr
        << " -h, --help              List the commands and options available"
        << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char* argv[]) {
    // settings
    std::string name;
    int channel = 0;
    std::string channelName;
    std::string configFile;

    // get default hardware config file from ~/.labRemote
    std::string homedir;
    if (getenv("HOME") == NULL)
        homedir = getpwuid(getuid())->pw_dir;
    else
        homedir = getenv("HOME");

    if (access((homedir + "/.labRemote/hardware.json").c_str(), F_OK) != -1)
        configFile = homedir + "/.labRemote/hardware.json";

    //
    // Parse command-line
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"name", required_argument, 0, 'n'},
            {"channel", required_argument, 0, 'c'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "n:c:e:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'n':
                name = optarg;
                break;
            case 'c':
                try {
                    channel = std::stoi(optarg);
                } catch (const std::invalid_argument& e) {
                    std::cerr << "Channel argument must be an integer, you "
                                 "provided \""
                              << optarg << "\"";
                    return 1;
                }
                break;
            case 'e':
                configFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (name.empty()) {
        std::cerr << "Required name argument missing." << std::endl;
        return 1;
    }

    std::string command;
    std::vector<std::string> params;
    if (optind < argc) {
        command = argv[optind++];
        std::transform(command.begin(), command.end(), command.begin(),
                       ::tolower);
        while (optind < argc) {
            std::string p(argv[optind++]);
            std::transform(p.begin(), p.end(), p.begin(), ::tolower);
            params.push_back(p);
        }
    } else {
        std::cerr << "Required command argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    // check if config file is provided, if not create JSON config
    EquipConf hw;
    if (configFile.empty()) {
        logger(logERROR)
            << "No input config file, either create default or use --equip.";
        return 1;
    } else {
        hw.setHardwareConfig(configFile);
    }

    logger(logDEBUG) << "Configuring meter \"" << name << "\"";
    std::shared_ptr<IMeter> meter = hw.getMeter(name);

    // Now interpret command
    logger(logDEBUG) << "Sending command to meter.";

    if (command == "meas-voltage") {
        if (logIt::loglevel >= logDEBUG) std::cout << "Voltage: ";
        std::cout << meter->measureDCV(channel);
        if (logIt::loglevel >= logDEBUG) std::cout << " V";
        std::cout << std::endl;
    } else if (command == "meas-current") {
        if (logIt::loglevel >= logDEBUG) std::cout << "Current: ";
        std::cout << meter->measureDCI(channel);
        if (logIt::loglevel >= logDEBUG) std::cout << " A";
        std::cout << std::endl;
    } else if (command == "meas-res") {
        bool use_4w = false;
        if (params.size() > 0) {
            if (params.size() != 1 || params[0] != "use4w") {
                logger(logERROR) << "Invalid number of parameters to command.";
                logger(logERROR) << "";
                usage(argv);
                return 1;
            }
            use_4w = true;
        }
        if (logIt::loglevel >= logDEBUG) std::cout << "Resistance: ";
        std::cout << meter->measureRES(channel, use_4w);
        if (logIt::loglevel >= logDEBUG) std::cout << " Ohms";
        std::cout << std::endl;
    } else if (command == "meas-cap") {
        if (logIt::loglevel >= logDEBUG) std::cout << "Capacitance: ";
        std::cout << meter->measureCAP(channel);
        if (logIt::loglevel >= logDEBUG) std::cout << " F";
        std::cout << std::endl;
    }
    logger(logDEBUG) << "All done.";
    return 0;
}
