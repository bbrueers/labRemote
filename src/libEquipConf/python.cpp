#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "DataSinkConf.h"
#include "EquipConf.h"
#include "IChiller.h"
#include "IMeter.h"
#include "IPowerSupply.h"
#include "PowerSupplyChannel.h"
#include "nlohmann/json.hpp"
#include "pybind11_json/pybind11_json.hpp"

namespace py = pybind11;
namespace nl = nlohmann;

void register_equipconf(py::module &m) {
    py::class_<EquipConf>(m, "EquipConf")
        .def(py::init<>())
        .def(py::init<const std::string &>())
        .def(py::init<const nl::json &>())
        .def("setHardwareConfig", (void (EquipConf::*)(const std::string &)) &
                                      EquipConf::setHardwareConfig)
        .def("setHardwareConfig", (void (EquipConf::*)(const nl::json &)) &
                                      EquipConf::setHardwareConfig)
        .def("getDeviceConf", &EquipConf::getDeviceConf)
        .def("getChannelConf", &EquipConf::getChannelConf)
        .def("getPowerSupply", &EquipConf::getPowerSupply)
        .def("getPowerSupplyChannel", &EquipConf::getPowerSupplyChannel)
        .def("getMeter", &EquipConf::getMeter)
        .def("getChiller", &EquipConf::getChiller);

    py::class_<DataSinkConf>(m, "DataSinkConf")
        .def(py::init<>())
        .def(py::init<const std::string &>())
        .def(py::init<const nl::json &>())
        .def("setHardwareConfig",
             (void (DataSinkConf::*)(const std::string &)) &
                 DataSinkConf::setHardwareConfig)
        .def("setHardwareConfig", (void (DataSinkConf::*)(const nl::json &)) &
                                      DataSinkConf::setHardwareConfig)
        .def("getDataSinkConf", &DataSinkConf::getDataSinkConf)
        .def("getDataSink", &DataSinkConf::getDataSink)
        .def("getDataStreamConf", &DataSinkConf::getDataStreamConf)
        .def("getDataStream", &DataSinkConf::getDataStream);
}
