from ._labRemote import com, ec, ps, datasink, devcom, chiller
from ._labRemote import incrDebug
from ._labRemote import setLogLevel
from ._labRemote import loglevel_e
from .version import version as __version__

import pkg_resources
import os

os.environ.setdefault(
    "LABREMOTE_RESOURCE_DIR",
    pkg_resources.resource_filename(__name__, f"share/{__name__}"),
)

__all__ = [
    "com",
    "ec",
    "ps",
    "datasink",
    "devcom",
    "chiller",
    "incrDebug",
    "setLogLevel",
    "loglevel_e",
    "__version__",
]
