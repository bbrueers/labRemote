include(GNUInstallDirs)

file(GLOB REGISTER_PYTHON_MODULE_SRC
          CONFIGURE_DEPENDS
          "${PROJECT_SOURCE_DIR}/src/*/python.cpp"
          )

file(GLOB LABREMOTE_PYTHON_SRC
          CONFIGURE_DEPENDS
          "${PROJECT_SOURCE_DIR}/src/labRemote/*.py"
          )

set(python_module_name _labRemote)
set(labremote_libs Com Utils PS EquipConf DataSink DevCom Chiller Meter nlohmann_json_schema_validator)

pybind11_add_module(${python_module_name} module.cpp ${REGISTER_PYTHON_MODULE_SRC})
target_link_libraries(${python_module_name} PRIVATE ${labremote_libs} pybind11_json)

if (SKBUILD)
    # Installing all python files to the root of the package
    install(FILES ${LABREMOTE_PYTHON_SRC} DESTINATION .)
    # Installing the extension module to the root of the package
    install(TARGETS ${python_module_name} LIBRARY DESTINATION .)
    # Installing dependent libs under the LIBDIR of the package
    install(TARGETS ${labremote_libs} LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

    # Set the RPATH
    if (APPLE)
      set(rpath "@loader_path")
    else()
      set(rpath "$ORIGIN")
    endif()
    set_target_properties(${python_module_name} PROPERTIES INSTALL_RPATH "${rpath}/${CMAKE_INSTALL_LIBDIR}")
    set_target_properties(${labremote_libs} PROPERTIES INSTALL_RPATH "${rpath}")
endif()
