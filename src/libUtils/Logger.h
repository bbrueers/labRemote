#ifndef LOGGER_H
#define LOGGER_H

// ####################
// Lightweight logger
// Author: Timon Heim
// Date: Feb 2017
// Notes:
// ###################

#include <iostream>
#include <sstream>

/* consider adding boost thread id since we'll want to know whose writting and
 * won't want to repeat it for every single call */

/* consider adding policy class to allow users to redirect logging to specific
 * files via the command line
 */

enum loglevel_e {
    logERROR = 0,
    logWARNING = 1,
    logINFO = 2,
    logDEBUG = 3,
    logDEBUG1 = 4,
    logDEBUG2 = 5,
    logDEBUG3 = 6,
    logDEBUG4 = 7
};

/** \brief Pretty formatted line of output
 */
class logIt {
 public:
    logIt(loglevel_e _loglevel = logERROR);
    ~logIt();

    template <typename T>
    logIt& operator<<(T const& value) {
        _buffer << value;
        return *this;
    }

 public:
    static loglevel_e loglevel;
    static bool includeTimestamp;
    static void setTimestamp(bool doit);

    /** \brief increment debug level of global logging up to maxium allowed
     */
    static void incrDebug();
    static void setLogLevel(loglevel_e level);

 private:
    std::ostringstream _buffer;
    static const std::string logString[8];
    static const std::string logStringColor[8];
};

#define logger(level)            \
    if (level > logIt::loglevel) \
        ;                        \
    else                         \
        logIt(level)

#endif
