#ifndef LABREMOTE_DATETIME_UTILS_H
#define LABREMOTE_DATETIME_UTILS_H

#include <chrono>
#include <ctime>    // localtime
#include <iomanip>  // setw, left
#include <sstream>
#include <stdexcept>
#include <string>

namespace utils {

//! Return the given timepoint as a formatted std::string
/*
 * The provided timepoint is formatted as std::string
 * with millisecond precision. The return std::string is
 * formatted as %H-%M-%S.ms.
 */
static inline std::string timestamp(std::chrono::system_clock::time_point t) {
    auto as_time_t = std::chrono::system_clock::to_time_t(t);
    struct tm tm;
    char buf[128];
    std::stringstream ts;
    if (::localtime_r(&as_time_t, &tm)) {
        if (std::strftime(buf, sizeof(buf), "%F %T", &tm)) {
            auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                t.time_since_epoch() % std::chrono::seconds{1});
            ts << std::string{buf};
            ts << "." << std::setw(3) << std::left << ms.count();
            return ts.str();
        }
    }
    throw std::runtime_error("timestamp failed to convert time point");
}

//! Return a formatted timestamp for the point in time when this function is
//! called
static inline std::string timestamp() {
    auto now = std::chrono::system_clock::now();
    return timestamp(now);
}

}  // namespace utils

#endif  // LABREMOTE_DATETIME_UTILS_H
