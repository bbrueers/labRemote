#ifndef Julabo_H
#define Julabo_H

#include <memory>
#include <stdexcept>
#include <string>

#include "IChiller.h"
#include "Logger.h"
#include "TextSerialCom.h"

//! \brief Object to interface with a Julabo series chiller
/**
 * # Example configuration for a Julabo Chiller:
 *
 *   {
 *     "name": "myJulaboChiller",
 *     "hw-type": "Chiller",
 *     "hw-model": "Julabo",
 *     "communication": {
 *        "protocol": "TextSerialCom",
 *        "termination": "\r",
 *        "returnTermination": "\n",
 *        "baudrate": "B4800",
 *        "port": "/dev/ttyUSB0",
 *        "charsize": "CS7",
 *        "parityBit": false,
 *        "flowControl": true
 *     }
 *   }
 *
 * The operator's manual for these chillers can be found at
 *  https://www.julabo.com/en-us/products/recirculating-coolers/fl-recirculating-coolers/fl1201
 */
class Julabo : public IChiller {
 public:
    /**
     * \param com The serial communication interface connected
     *   to the chiller
     */
    Julabo(const std::string& name);
    ~Julabo() = default;

    //! Initialize the serial communication channel
    void init();
    //! Turn the chiller on
    void turnOn();
    //! Turn the chiller off
    void turnOff();
    //! Set the target temperature of the chiller
    /**
     * \param temp The target temperature in Celsius
     */
    void setTargetTemperature(float temp);
    //! Return the temperature that the chiller is set to in Celsius
    float getTargetTemperature();
    //! Return the current temperature of the chiller in Celsius
    float measureTemperature();
    //! Get the status of the chiller
    /**
     * \return true if chiller is in "run" mode, and false if
     *   it's in "standby" mode
     */
    bool getStatus();
};

#endif
