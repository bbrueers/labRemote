#ifndef SPSRC211_H
#define SPSRC211_H

#include <memory>
#include <stdexcept>
#include <string>

#include "IChiller.h"
#include "Logger.h"
#include "TextSerialCom.h"

//! \brief Object to interface with a SPSRC211 cooler series
//! chiller
/**
 * The TextSerialCom passed to this object should already be
 * initialized, be set to the same baud rate as the chiller,
 * and have the termination set to "\r".
 *
 * # Example
 *
 * ```
 * std::shared_ptr<TextSerialCom> com =
 *   std::make_shared<TextSerialCom>("/dev/ttyUSB0",B9600);
 * com->setTermination("\r");
 * com->init();
 *
 * SPSRC211 chiller;
 *
 * chiller.setCom(com);
 * chiller.init();
 * chiller.setSetTemperature(15.0);
 * ```
 *
 * The operator's manual for these chillers can be found at
 * https://twiki.cern.ch/twiki/pub/Sandbox/EquipmentAndInstallationThermalImagingSetUp/Manual_RC211_MNL-011-A_RC211_Rev006_0412.pdf
 *
 */
class SPSRC211 : public IChiller {
 public:
    /**
     *  \param name: name of the chiller
     */
    SPSRC211(const std::string& name);
    ~SPSRC211() = default;

    //! Initialize the serial communication channel
    void init();
    //! Turn the chiller on
    void turnOn();
    //! Turn the chiller off
    void turnOff();
    //! Set the ramp rate (degree/minute) of the chiller
    /**
     * \param RR The target ramp rate in degree/minute
     */
    void setRampRate(float RR);
    //! Return the ramp rate that the chiller is set to in degree/minute
    float getRampRate();

    //! Set the target temperature of the chiller
    /**
     * \param temp The target temperature in Celsius
     */
    void setTargetTemperature(float temp);
    //! Return the temperature that the chiller is set to in Celsius
    float getTargetTemperature();
    //! Return the current temperature of the chiller in Celsius
    float measureTemperature();
    //! Get the status of the chiller
    /**
     * \return true if chiller is in "run" mode, and false if
     *   it's in "standby" mode
     */
    bool getStatus();
    //! Returns error fault codes
    /**
     * \return 0 if system OK, and an error code otherwise
     */
    int getFaultStatus();

 protected:
 private:
    //! Send a command to the chiller
    /**
     * This function will send cmd to chiller and receive the response.
     * It will keep receiving response until a '!' is received at the end.
     * The function will also check for error code in the response.
     * It will throw runtime_error in case an error message is received,
     * with E041 and E042 as exception (ignored).
     *
     * \return the response from chiller after this cmd
     */
    std::string command(const std::string& cmd);
    //! Extract the float from the output of the chiller, example output could
    //! be F044=+0021.78!
    float parseString(const std::string& output);
};

#endif
