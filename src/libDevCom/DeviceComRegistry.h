#ifndef DEVICE_COM_REGISTRY_H
#define DEVICE_COM_REGISTRY_H
#include <map>
#include <string>
#include <vector>

#include "ClassRegistry.h"

namespace DeviceComRegistry {
bool registerDeviceCom(const std::string& device_name,
                       const std::string& device_type);
std::map<std::string, std::vector<std::string>> listDevCom();
std::vector<std::string> listDeviceTypes();
std::vector<std::string> listDevices();
}  // namespace DeviceComRegistry

#define REGISTER_DEVCOM(name, type)         \
    static bool _registered_##type_##name = \
        DeviceComRegistry::registerDeviceCom(#name, #type);
#endif
