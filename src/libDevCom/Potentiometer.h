#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

#include <memory>

#include "CalibratedDevice.h"
#include "DummyCalibration.h"

//! \brief Abstract implementation of a potentiometer
/**
 * The read and write functions correspond to setting the wiper-to-B
 * resistance.
 */
class Potentiometer : public CalibratedDevice {
 public:
    /**
     * \param calibration Device calibration to convert resistances to tap
     * positon
     */
    Potentiometer(std::shared_ptr<DeviceCalibration> calibration =
                      std::make_shared<DummyCalibration>());
    virtual ~Potentiometer() = default;
};

#endif  // POTENTIOMETER_H
