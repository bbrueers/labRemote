#ifndef DUMMYCALIBRATION_H
#define DUMMYCALIBRATION_H

#include "DeviceCalibration.h"

//! \brief Calibration that assumes `counts = value`.
class DummyCalibration : public DeviceCalibration {
 public:
    virtual double calibrate(int32_t counts);
    virtual int32_t uncalibrate(double value);
};

#endif  // DUMMYCALIBRATION_H
