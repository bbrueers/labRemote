#include "ADCDevComuino.h"

#include "DeviceComRegistry.h"
#include "LinearCalibration.h"
REGISTER_DEVCOM(ADCDevComuino, ADCDevice)

ADCDevComuino::ADCDevComuino(double reference,
                             std::shared_ptr<TextSerialCom> com)
    : ADCDevice(std::make_shared<LinearCalibration>(reference, 1024)),
      m_com(com) {}

ADCDevComuino::~ADCDevComuino() {}

int32_t ADCDevComuino::readCount(uint8_t ch) {
    std::stringstream cmd;
    cmd << "ADC " << std::dec << (int)ch;
    std::string response = m_com->sendreceive(cmd.str());
    std::stringstream ss;
    ss << std::dec << response;
    uint32_t count;
    ss >> count;
    return count;
}

int32_t ADCDevComuino::readCount() {
    logger(logDEBUG) << "No pin number specified. Reading A0.";
    return readCount(0);
}

void ADCDevComuino::readCount(const std::vector<uint8_t>& chs,
                              std::vector<int32_t>& counts) {
    counts.clear();
    for (uint8_t chi = 0; chi < chs.size(); chi++) {
        counts.push_back(readCount(chs[chi]));
    }
}
