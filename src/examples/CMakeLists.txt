# setup the output directory for the executables
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/examples)

# control powersupply
add_executable(control_powersupply_example)
target_sources(control_powersupply_example PRIVATE control_powersupply_example.cpp)
target_link_libraries(control_powersupply_example PRIVATE Utils PS EquipConf)

# spam powersupply
add_executable(spam_powersupply)
target_sources(spam_powersupply PRIVATE spam_powersupply.cpp)
target_link_libraries(spam_powersupply PRIVATE Utils PS EquipConf)

# fluke8842
add_executable(fluke8842_example)
target_sources(fluke8842_example PRIVATE fluke8842_example.cpp)
target_link_libraries(fluke8842_example PRIVATE Utils Meter Com)

# multimeter
add_executable(multimeter_example)
target_sources(multimeter_example PRIVATE multimeter_example.cpp)
target_link_libraries(multimeter_example PRIVATE Utils Meter Com)

# scope
if ( ${libScope_FOUND} )
    add_executable(scope_example)
    target_sources(scope_example PRIVATE scope_example.cpp)
    target_link_libraries(scope_example PRIVATE Utils Scope)
    target_compile_definitions(scope_example PRIVATE SCOPE=1)
endif()

# datasink
add_executable(datasink_example)
target_sources(datasink_example PRIVATE datasink_example.cpp)
target_link_libraries(datasink_example PRIVATE Utils EquipConf)

# devcomuino
add_executable(devcomuino_example)
target_sources(devcomuino_example PRIVATE devcomuino_example)
target_link_libraries(devcomuino_example PRIVATE Com Utils)

# ntc
add_executable(ntc_example)
target_sources(ntc_example PRIVATE ntc_example.cpp)
target_link_libraries(ntc_example PRIVATE Com DevCom Utils)

# sht85
if ( ${ENABLE_FTDI} )
    add_executable(sht85_example)
    target_sources(sht85_example PRIVATE sht85_example.cpp)
    target_link_libraries(sht85_example PRIVATE DevCom Utils)
endif()

# ftdi adc
if ( ${ENABLE_FTDI} )
    add_executable(ft232h_adc_example)
    target_sources(ft232h_adc_example PRIVATE ft232h_adc_example.cpp)
    target_link_libraries(ft232h_adc_example PRIVATE Utils DevCom)
    #target_compile_definitions(ft232h_adc_example PUBLIC FTDI=1)
endif()

# uio
add_executable(uio_example)
target_sources(uio_example PRIVATE uio_example.cpp)
target_link_libraries(uio_example PRIVATE DevCom Com Utils)

# Si7021 example
add_executable(Si7021_example)
target_sources(Si7021_example PRIVATE Si7021_example.cpp)
target_link_libraries(Si7021_example PRIVATE Utils DevCom)

# temp hum monitor
add_executable(temp_hum_monitor_example)
target_sources(temp_hum_monitor_example PRIVATE temp_hum_monitor_example.cpp)
target_link_libraries(temp_hum_monitor_example PRIVATE Com DevCom EquipConf)

# tempsensor monitor
if ( ${ENABLE_FTDI} )
    add_executable(tempsensor_monitor_example)
    target_sources(tempsensor_monitor_example PRIVATE tempsensor_monitor_example.cpp)
    target_link_libraries(tempsensor_monitor_example PRIVATE DevCom)
endif()
