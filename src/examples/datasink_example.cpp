#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <thread>

#include "DataSinkConf.h"
#include "IDataSink.h"
#include "Logger.h"

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " configfile.json streamName"
              << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -d, --debug       Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "" << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    // Parse command-line
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {{"debug", no_argument, 0, 'd'},
                                               {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (optind > argc - 2) {
        logger(logERROR) << "Missing positional arguments.";
        usage(argv);
        return 1;
    }

    std::string configFile = argv[optind++];
    std::string streamName = argv[optind++];

    logger(logDEBUG) << "Settings:";
    logger(logDEBUG) << " Config file: " << configFile;
    logger(logDEBUG) << " Stream name: " << streamName;

    // Crease stream
    DataSinkConf ds;
    ds.setHardwareConfig(configFile);
    std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

    //
    // Measurement loop

    std::random_device rd{};
    std::mt19937 gen{rd()};

    std::normal_distribution<> d0{20, 1};
    stream->setTag("location", "room");
    for (uint32_t i = 0; i < 4; i++) {
        stream->startMeasurement("climate", std::chrono::system_clock::now());
        stream->setField("temperature", d0(gen));
        stream->recordPoint();
        stream->endMeasurement();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    stream->setTag("location", "fridge");
    std::normal_distribution<> d1{0, 1};
    for (uint32_t i = 0; i < 4; i++) {
        stream->startMeasurement("climate", std::chrono::system_clock::now());
        stream->setField("temperature", d1(gen));
        stream->recordPoint();
        stream->endMeasurement();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}
