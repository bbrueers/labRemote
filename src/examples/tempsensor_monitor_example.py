from labRemote import devcom

from argparse import ArgumentParser
import sys, inspect
from datetime import datetime
import time
import logging
logging.basicConfig()
logger = logging.getLogger("tempsensor_monitor_example")

def tempsensor_monitor_example() :

    ##
    ## configure and initialize the FT232H device
    ##
    mpsse = devcom.MPSSEChip
    ft232 = devcom.FT232H(mpsse.Protocol.I2C, mpsse.Speed.ONE_HUNDRED_KHZ, mpsse.Endianness.MSBFirst, "", "")
    i2c = devcom.I2CFTDICom(ft232, 0x27)

    sensor = devcom.HIH6130(i2c)

    while True :
        time.sleep(1)
        sensor.read()
        logger.info(f"[{datetime.now()}] Sensor status: {sensor.status()}, humidity: {sensor.humidity()}, temperature: {sensor.temperature()}")

if __name__ == "__main__" :

    parser = ArgumentParser(description = "Example of I2C-based temperature sensor monitoring through the FTDI-based FT232H chip")
    parser.parse_args()

    ##
    ## first check that hte FTDI support as been enabled in the current labRemote build
    ##
    if not inspect.isclass(devcom.I2CFTDICom) :
        logger.error("FTDI support has not been enabled")
        sys.exit(1)
    tempsensor_monitor_example()
