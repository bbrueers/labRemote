#include "Fluke8842.h"

#include "IMeter.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
REGISTER_METER(Fluke8842)

Fluke8842::Fluke8842(const std::string& name) : IMeter(name, {"Fluke8842"}) {}

bool Fluke8842::ping(unsigned dev) {
    std::string result = "";
    if (dev == 0) {
        logger(logDEBUG) << "ping the multimeter.....";
        result = this->sendreceive("G8\r\n");
    } else {
        throw std::runtime_error("Other channels not implemented! ");
    }
    return !result.empty();
}

std::string Fluke8842::identify() {
    std::string idn = this->sendreceive("G8");
    return idn;
}

void Fluke8842::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Initialising: ";
    this->send("*");
}

std::string Fluke8842::GetMode() { return this->sendreceive("G0"); }

void Fluke8842::send(std::string cmd) {
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send(cmd);
    std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string Fluke8842::sendreceive(std::string cmd) {
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send(cmd);
    std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
    std::string buf = m_com->receive();
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
    return buf;
}

std::string Fluke8842::GetValue() { return this->sendreceive("?"); }

double Fluke8842::measureDCV(unsigned channel) {
    ScopeLock lock(m_com);
    std::string CurrentMode = this->GetMode();
    if (CurrentMode != "1012") {
        logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                 ", Reset the meter to 1012";
        this->reset();
        this->SetMode(FlukeMode::VOLTAGEDC);
    }

    if (channel > 0)  // use scanner card; not implemented for Fluke 45
        std::cerr << "Unimplemented channel number: " << channel << std::endl;
    return std::stod(this->GetValue());
}

double Fluke8842::measureDCI(unsigned channel) {
    ScopeLock lock(m_com);
    std::string CurrentMode = this->GetMode();
    if (CurrentMode != "5012") {
        logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                 ", Reset the meter to 5012";
        this->reset();
        this->SetMode(FlukeMode::CURRENTDC);
    }

    if (channel > 0)  // use scanner card; not implemented for Fluke 45
        std::cerr << "Unimplemented channel number: " << channel << std::endl;

    std::string val = this->GetValue();
    return std::stod(val);
}

double Fluke8842::measureRES(unsigned channel, bool use4w) {
    ScopeLock lock(m_com);
    std::string CurrentMode = this->GetMode();
    if (use4w) {
        if (CurrentMode != "4012") {
            logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                     ", Reset the meter to 4012";
            this->reset();
            this->SetMode(FlukeMode::FOHMS);
        }
    } else if (CurrentMode != "3012") {
        logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                 ", Reset the meter to 3012";
        this->reset();
        this->SetMode(FlukeMode::OHMS);
    }

    if (channel > 0)  // use scanner card; not implemented for Fluke 8842
        std::cerr << "Unimplemented channel number: " << channel << std::endl;

    std::string val = this->GetValue();
    return std::stod(val);
}

double Fluke8842::measureCAP(unsigned channel) {
    std::cerr << "Unable to measure capacitance by Fluke 8842, exit. "
              << std::endl;
    return -1.00;
}

void Fluke8842::SetMode(enum FlukeMode mode) {
    switch (mode) {
        case FlukeMode::VOLTAGEDC:
            this->send("F1 R0 S1 T2");
            break;
        case FlukeMode::CURRENTDC:
            this->send("F5 R0 S1 T2");
            break;
        case FlukeMode::OHMS:
            this->send("F3 R0 S1 T2");
            break;
        case FlukeMode::FOHMS:
            this->send("F4 R0 S1 T2");
            break;
        default:
            logger(logERROR) << __PRETTY_FUNCTION__ << " : Unknown mode!";
            break;
    }
}

void Fluke8842::checkCompatibilityList() {
    // get model connected to the meter
    // std::string idn = this->identify();
    std::string idn = "FLUKE,8842A,0,V4.0 CR LF";
    // get list of models
    std::vector<std::string> models = IMeter::getListOfModels();
    logger(logWARNING) << "Get meter identification command not working. "
                          "Bypassing identification check.";

    if (models.empty()) {
        logger(logINFO) << "No model identifier implemented for this meter. No "
                           "check is performed.";
        return;
    }

    std::size_t pos = m_name.find("Fluke");
    std::string brand = m_name.substr(pos, pos + 5);
    std::string type = m_name.substr(pos + 5, pos + 9);

    for (int i = 0; i < brand.length(); i++) {
        brand[i] = toupper(brand[i]);
    }

    for (const std::string& model : models) {
        if (idn.find(brand) != std::string::npos &&
            idn.find(type) != std::string::npos)
            return;
    }
    logger(logERROR) << "Unknown meter: " << idn;
    logger(logERROR) << "Unknown meter: " << idn;
    throw std::runtime_error("Unknown meter: " + idn);
}