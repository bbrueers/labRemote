#include "RigolDM30XX.h"

#include "IMeter.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
#include "StringUtils.h"
REGISTER_METER(RigolDM30XX)

RigolDM30XX::RigolDM30XX(const std::string& name)
    : IMeter(name, {"DM3058", "DM3058E", "DM3068"}) {}

bool RigolDM30XX::ping(unsigned /*dev*/) {
    std::string result = "";
    logger(logDEBUG) << "ping the multimeter.....";
    result = m_com->sendreceive("*IDN?");
    utils::rtrim(result);
    if (result.empty()) {
        throw std::runtime_error("Failed communication with the device");
    } else {
        logger(logDEBUG) << result;
    }

    return !result.empty();
}

std::string RigolDM30XX::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    return idn;
}

void RigolDM30XX::autowait() {
    m_com->send("*OPC");
    int ESRValue = 0;
    while ((ESRValue & 1) == 0) {
        ESRValue = std::stoi(m_com->sendreceive("*ESR?"));
        std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
    }
}

void RigolDM30XX::send(const std::string& cmd) {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send("*CLS");
    m_com->send(cmd);
    this->autowait();
}

std::string RigolDM30XX::sendreceive(const std::string& cmd) {
    m_com->send("*CLS");
    std::string buf = m_com->sendreceive(cmd);
    this->autowait();
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
    utils::rtrim(buf);
    return buf;
}

void RigolDM30XX::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Initialising: ";
    this->send("*RST");
}

// measure DC voltage with high precision
// take average of 10 repeatings
double RigolDM30XX::measureDCV(unsigned channel) {
    ScopeLock lock(m_com);

    return std::stod(this->sendreceive(":MEAS:VOLT:DC?"));
}

// measure resistance (2W or 4W)
// take average of 10 repeatings
double RigolDM30XX::measureRES(unsigned channel, bool use4w) {
    std::string n_func = "RES";
    if (use4w) n_func = "FRES";

    ScopeLock lock(m_com);

    return std::stod(this->sendreceive(":MEAS:" + n_func + "?"));
}

// measure DC current with high precision
// take average of 10 repeatings
double RigolDM30XX::measureDCI(unsigned channel) {
    ScopeLock lock(m_com);

    return std::stod(this->sendreceive(":MEAS:CURR:DC?"));
}
