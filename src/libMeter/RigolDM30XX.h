#ifndef RigolDM30XX_H
#define RigolDM30XX_H
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "IMeter.h"

/*
 RigolDM30XX single-channel multimeter
 Author: Simon Koch
 Date: Feb 2024
 Reference 1 (User Guide, DM3068):
 https://www.rigol-uk.co.uk/pdf/Rigol-DM3068-User-Guide.pdf
 Reference 2 (Programmers Guide, DM3058/3058E/3068):
 https://beyondmeasure.rigoltech.com/acton/attachment/1579/f-003f/0/-/-/-/-/file.pdf

 Based on implementation of Keithley2000
   - channel argument included only for compatibility with IMeter interface
*/
class RigolDM30XX : public IMeter {
 public:
    RigolDM30XX(const std::string& name);

    /** ping the device
     */
    virtual bool ping(unsigned dev = 0);

    virtual std::string identify();

    virtual void reset();

    /** measure DC voltage (unit: V)
     */
    virtual double measureDCV(unsigned channel = 0);

    /** measure DC current (unit: A)
     */
    virtual double measureDCI(unsigned channel = 0);

    /* measure resistance (unit: Ohm)
     */
    virtual double measureRES(unsigned channel = 0, bool use4w = false);

 private:
    void send(const std::string& cmd);

    std::string sendreceive(const std::string& cmd);
    void autowait();

    std::chrono::milliseconds m_wait{10};
};

#endif
