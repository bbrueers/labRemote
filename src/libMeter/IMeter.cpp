#include "IMeter.h"

#include "Logger.h"

IMeter::IMeter(const std::string& name, std::vector<std::string> models) {
    m_name = name;
    m_models = models;
}

void IMeter::setCom(std::shared_ptr<ICom> com) {
    if (!com->is_open()) com->init();

    m_com = com;
    if (!ping(0))
        throw std::runtime_error("Failed communication with the multimeter");
}

bool IMeter::ping(unsigned dev) {
    logger(logWARNING) << "ping() not implemented for this multimeter.";
    return false;
}

void IMeter::setConfiguration(const nlohmann::json& /*config*/) {
    // default is to do nothing
}

void IMeter::checkCompatibilityList() {
    // get model connected to the meter
    std::string idn = identify();

    // get list of models
    std::vector<std::string> models = IMeter::getListOfModels();

    if (models.empty()) {
        logger(logINFO) << "No model identifier implemented for this meter. No "
                           "check is performed.";
        return;
    }

    for (const std::string& model : models) {
        if (idn.find(model) != std::string::npos) return;
    }
    return;
    // logger(logERROR) << "Unknown meter: " << idn;
    // throw std::runtime_error("Unknown meter: " + idn);
}

std::vector<std::string> IMeter::getListOfModels() { return m_models; }

double IMeter::measureDCV(unsigned channel) {
    logger(logWARNING)
        << "IMeter::measureDCV() not implemented for this multimeter (name=\""
        << m_name << ").";
    return 0;
}

std::vector<double> IMeter::measureDCV(const std::vector<unsigned>& channels) {
    logger(logWARNING)
        << "IMeter::measureDCV() not implemented for this multimeter (name=\""
        << m_name << ").";
    return {};
}

double IMeter::measureRES(unsigned channel, bool use4w) {
    logger(logWARNING)
        << "IMeter::measureRES() not implemented for this multimeter (name=\""
        << m_name << ").";
    return 0;
}

std::vector<double> IMeter::measureRES(const std::vector<unsigned>& channels,
                                       bool use4w) {
    logger(logWARNING)
        << "IMeter::measureRES() not implemented for this multimeter (name=\""
        << m_name << ").";
    return {};
}

double IMeter::measureCAP(unsigned channel) {
    logger(logWARNING)
        << "IMeter::measureCAP() not implemented for this multimeter (name=\""
        << m_name << ").";
    return 0;
}

std::vector<double> IMeter::measureCAP(const std::vector<unsigned>& channels) {
    logger(logWARNING)
        << "IMeter::measureCAP() not implemented for this multimeter (name=\""
        << m_name << ").";
    return {};
}

double IMeter::measureDCI(unsigned channel) {
    logger(logWARNING)
        << "IMeter::measureDCI() not implemented for this multimeter (name=\""
        << m_name << ").";
    return 0;
}

std::vector<double> IMeter::measureDCI(const std::vector<unsigned>& channels) {
    logger(logWARNING)
        << "IMeter::measureDCI() not implemented for this multimeter (name=\""
        << m_name << ").";
    return {};
}
