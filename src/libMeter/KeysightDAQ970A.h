#ifndef METER_KEYSIGHT_DAQ970A_H
#define METER_KEYSIGHT_DAQ970A_H

#include <chrono>
#include <string>
#include <vector>

#include "IMeter.h"
class ICom;

// clang-format off
/** \brief Implementation for benchtop Keysight DAQ970A, with DAQM901A modules
 *
 * The `KeysightDAQ970A` `IMeter` implementation adds support for the Keysight DAQ970A
 * benchtop DAQ with DAQM901A general purpose plug-in multiplexer modules.
 * General information about the Keysight DAQ970A can be found [here]
 * (https://www.keysight.com/us/en/assets/7018-06259/technical-overviews/5992-3168.pdf),
 * and specific programming information can be found [here]
 * (https://www.keysight.com/us/en/assets/9018-04756/programming-guides/9018-04756.pdf).
 *
 * Each DAQM901A multiplexer module offers 20 channels capable of measuring
 * AC/DC voltage, resistance, and capacitance. There are an additional two channels
 * capable of measuring AC/DC currents (channels 21 and 22).
 * The Keysight DAQ970A has 3 plug-in ports: port 100, port 200, and port 300.
 * DAQM901A modules plugged into port 100 have channel numbers 101-122.
 * DAQM901A modules plugged into port 200 have channel numbers 201-222.
 * DAQM901A modules plugged into port 300 have channel numbers 301-322.
 *
 * Support for single measurements is provided by following the standard `IMeter`
 * use, offering implementation for `measureDCV`, `measureDCI`, `measureRES`,
 * and `measureCAP`. An example for measuring DC voltages is as follows:
 *
 *   std::shared_ptr<KeysightDAQ970A> meter = ...; // get instance of KeysightDAQ970A
 *
 *   // read a single channel
 *   unsigned channel{101};
 *   double value = meter->measureDCV(channel);
 *
 *   // configure the Keysight DAQ970A to measure from multiple channels at once
 *   std::vector<unsigned> channels{101, 102, 103, 104};
 *   std::vector<double> values = meter->measureDCV(channels);
 *
 * When reading from multiple channels, as in the above, the input channels list
 * should be monotonically increasing since the Keysight DAQ970A hardware
 * scans through channels in this order and orders the returned data in this order,
 * regardless of the order of channels provided to it.
 *
 * Support for measurement of several sample statistics is offered: average,
 * standard deviation, maximum, minimum, and peak-to-peak. Measurement
 * of sample statistics is supported by enabling the statistics measurement
 * and configuring the Keysight DAQ970A trigger count to be greater than 1 (the
 * default trigger count). This is done as follows:
 *
 *   std::shared_ptr<KeysightDAQ970A> meter = ...; // get instance of KeysightDAQ970A
 *   meter->setTrigCount(10); // compute sample statistics across 10 measurements per channel
 *
 *   // compute sample statistics across a single channel
 *   unsigned channel{101};
 *   double average_val = meter->measureDCV(channel).at(0);
 *   double std_dev_val = meter->getStdDeviation().at(0);
 *   double min_val = meter->getMinimum().at(0);
 *   double max_val = meter->getMaximum().at(0);
 *
 *   // compute the sample statistics across multiple channels
 *   std::vector<unsigned> channels{101,102,103,104};
 *   std::vector<double> average_values = meter->measureDCV(channels);
 *   std::vector<double> std_dev_values = meter->getStdDeviation();
 *   std::vector<double> min_values = meter->getMinimum();
 *   std::vector<double> max_values = meter->getMaximum();
 *
 * As seen in the above snippet, when the sample statistics computation have been
 * enabled (via setting the trigger count to be larger than 1, eg `setTrigCount(10)`), the methods `measureDCV`,
 * `measureDCI`, `measureRES`, and `measureCAP` will return the computed
 * averages. The other sample statistics computed from the call to `measureX`,
 * are available via subsequent calls to `KeysightDAQ970A::getStdDeviation()`,
 * `KeysightDAQ970A::getMaximum()`, and `KeysightDAQ970A::getMinimum()`.
 * These `get` methods for the sample statistics always hold the values from the
 * most recent call to `measureX`.
 * Also note that the sample statistic `get` methods (e.g. `KeysightDAQ970A::getStdDeviation()`)
 * always return a `std::vector<double>` even when the corresponding
 * measurement was only done across a single channel (in which case, the size
 * of the `std::vector` will be 1).
 *
 * The sample statistics computation (enabled via `KeysightDAQ::setTrigCount(n)` with `n>1`)
 * are computed within each channel, not across channels.
 * That is, if you configure for 10 triggers and measure channel 101 and 102
 * and channel 101 always measures a value of 0 and channel 102 always measures a value of
 * 5, the `std::vector<double>` returned by,
 *    std::vector<double> average_values = meter->measureDCV({101, 102});
 * will have a value of 0 at element 0 and a value of 5 at element 1
 * (likewise for the other sample statistics).
 *
 */
// clang-format on

class KeysightDAQ970A : public IMeter {
 public:
    KeysightDAQ970A(const std::string& name);

    virtual bool ping(unsigned dev = 0);
    virtual std::string identify();
    virtual void reset();

    virtual double measureDCV(unsigned channel = 0);
    virtual std::vector<double> measureDCV(
        const std::vector<unsigned>& channels);

    virtual double measureDCI(unsigned channel = 0);
    virtual std::vector<double> measureDCI(
        const std::vector<unsigned>& channels);

    virtual double measureRES(unsigned channel = 0, bool use4w = false);
    virtual std::vector<double> measureRES(
        const std::vector<unsigned>& channels, bool use4w = false);

    virtual double measureCAP(unsigned channel = 0);
    virtual std::vector<double> measureCAP(
        const std::vector<unsigned>& channels);

    //! \brief Specific statistics that can be computed during a measurement
    //! scan
    enum class Statistic {
        All,
        Average,
        StdDeviation,
        Maximum,
        Minimum,
        PeakToPeak
    };

    // clang-format off
    //! \brief Set the number of triggers for statistics measurementts
    /**
        Configures the Keysight DAQ970A trigger count.

        If the trigger count is set to any value greater than 1,
        this enables the computation of the statistics quantities.

     *  When the statistics measurements are enabled, each of the `measureDCV`,
     *  `measureDCI`, `measureRES`, and `measureCAP` methods will return
     *  the average value over the number of measurements set by the
     *  `setTrigCount` method. The other statistics will be available after
     *  calling each of the `measureX` methods via the `getStdDeviation`,
     *  `getMaximum`, and `getMinimum` methods (there is also
     *  a `getAverage` method which returns the same information
     *  as the original call to the `measureX` method).
     *  That is, the following is the recipe for accessing the statistics
     * measurements:
     *
     *   1. Call `KeysightDAQ970A::setTrigCount(number)`
     *   2. Call `KeysightDAQ970A::measureDCV(channels)`, which will return the
     * average values of each channels' measurements
     *   3. Call `KeysightDAQ970A::getStdDeviation()`, which will return the
     * standard deviation across each channel for the measurements performed in
     * step #3
     *   4. Call `KeysightDAQ970A::getMaximum()`, which will return the maximum
     * values across each channel for the measurements performed in step #3
     *   5. Call `KeysightDAQ970A::getMinimum()`, which will return the minimum
     * values across each channel for the measurements performed in step #3
     *
     *  Note that there is no need to call `KeysightDAQ970A::getAverage` as it
     * returns the same information as what is returned by the
     * `Keysight::measureX` methods (step #3 in the above), but it is available
     * for completeness.
     *
     *  If `KeysightDAQ970A::setMeasurementStat` is called with any other method
     * than the `Statistic::All`, then the values returned by the
     * `KeysightDAQ970A::measureX` method will return the values associated with
     * the specified statistic. By default, an instance of `KeysightDAQ970A` is
     * set to measure all statistics as if `KeysightDAQ970A::setMeasurementStat`
     * has been called with `Statistic::All`.
     *
     * The trigger count set by `setTrigCount` is ignored unless the
     * statistics measurements are enabled.

        \param trig_count The number of triggers to acquire from each channel
       when a statistics measurement is performed
     */
    // clang-format on
    void setTrigCount(unsigned trig_count = 1) { m_trig_count = trig_count; }

    // clang-format off
    //! \brief Set which measurement statistic to measure
    /* 
     * Specify a specific set of statistics to measure when computing the
     * measurement statistics (enabled with `KeysightDAQ970A::setDoStat(true)`.
     *
     * By default, an instance of `KeysightDAQ970A` will compute all statistics
     * possible (average, standard deviation, maximum, and minimum), but this method
     * can be used to specify a single statistic to be computed (unless one passes
     * `Statistic::All`, which will revert back to the default behavior).
     *
     * \param stat A value of the `KeysightDAQ970A::Statistic` enum.
     */
    // clang-format on
    void setMeasurementStat(Statistic stat) { m_measurement_stat = stat; }

    //! \brief Get the average value statistic for all the previous channel
    //! measurements
    std::vector<double> getAverage() { return m_average_last; }

    //! \brief Get the standard deviation statistic for all the previous channel
    //! measurements
    std::vector<double> getStdDeviation() { return m_stddev_last; }

    //! \brief Get the maximum value statistic for all the previous channel
    //! measurements
    std::vector<double> getMaximum() { return m_max_last; }

    //! \brief Get the minimum value statistic for all the previous channel
    //! measurements
    std::vector<double> getMinimum() { return m_min_last; }

 private:
    // these methods will be moved to a SCPI interface for meters
    void send(const std::string& cmd, bool wait_for_opc = true);
    std::string sendreceive(const std::string& cmd);

    //| \brief Method to query the OPC status
    void waitForOPC();

    //! \brief Method to poll the event status register
    /**
     * This method continuously polls the ESR of the meter, and returns
     * once it indicates that all command previous to this (including OPC)
     * have been handled.
     *
     * This method is useful when performing measurements across
     * many channels when the trigger count is greater than 1.
     * Without polling, the usual scheme of querying the OPC may
     * hang and the serial device may timeout.
     */
    void waitForEventCompletion();

    std::chrono::milliseconds m_wait{10};

    // the valid types of measurements that can be made
    enum class Function {
        VoltageDC,
        CurrentDC,
        Resistance,
        Resistance4W,
        Capacitance
    };

    Statistic m_measurement_stat = Statistic::All;
    unsigned m_trig_count = 1;
    void clearStatMeasurements();
    std::vector<double> m_average_last;
    std::vector<double> m_stddev_last;
    std::vector<double> m_max_last;
    std::vector<double> m_min_last;

    // method for initiating the measurements
    std::vector<double> measure(std::vector<unsigned> channels, Function func);

    // methods for configuring the channels for specific functions
    void configDCV(const std::string& channelListString);
    void configDCI(const std::string& channelListString);
    void configRES(const std::string& channelListString, bool use4w = false);
    void configCAP(const std::string& channelListString);

    // clang-format off
    //! \brief Convert a response from the meter into a set of measurement values
    /**
     *  Measurement responses from the KeysightDAQ970A are returned as a comma-delimited
     *  string, with each measurement separated from the other by a comma.
     *  For example, a response of the form:
     *      "1.23,0.4,1.0"
     *  would correpond to three measurements.
     *  This method takes that response and splits the string by the comma delimeters
     *  and converts each of the resulting fields into a value (double).
     *
     *  This method throws an expection of the returned response does not
     *  contained the expected number of measurement samples. That is,
     *  if you configured 10 channels for a measurement and the response
     *  string only had 4 comma-delimited samples, an exception will be thrown.
     *
     *  \param response The response from the meter
     *  \param n_expected The number of measurements you expect to be in the response string
     */
    // clang-format on
    std::vector<double> samplesFromResponse(const std::string& response,
                                            unsigned n_expected);

    // clang-format off
    //! \brief Ensure that a vector of unsigned integers is in monotonically increasing order
    /**
     *  When configuring the KeysightDAQ970A to perform a measurement/scan over multiple
     *  channels at once, the comma-delimited samples that are returned from the
     *  meter are always in increasing order. This is based on how the actual
     *  meter's hardware performs the measurement scan.
     *  This method is used to check that any channels list that a user provides
     *  is in this same hardware order so that whatever channels list the user
     *  is using has a 1:1 correspondence with the channel ordering assumed by the
     *  hardware.
     *
     *  \param channels A list of channels
     **/
    // clang-format on
    bool channelsAreIncreasingOrder(const std::vector<unsigned>& channels);

    // clang-format off
    //! \brief Format a list of channels into the SCPI channel list 
    /**
     *  When configuring multiple channels at once with a single SCPI command,
     *  the channels to be configured are formatted in the following way,
     *      "(@M,N,O,P)"
     *  That is, a comma-delimited list enclosed by "(@...)".
     *  This function takes a list of channels and formats the corresponding
     *  string as described above.
     *
     *  \param channels The list of channels
     */
    // clang-format on
    std::string generateChannelListString(std::vector<unsigned> channels);

    // clang-format off
    //! \brief Check that the list of channels is valid for a given meter function
    /**
     *  On a given plug-in module for the KeysightDAQ970A, certain channels are
     *  restricted to certain functionalities.
     *  This function checks that the user-provided set of channels can be configured
     *  for the user-provided function.
     *
     *  For example, for the DAQM901A plug-in module that the class KeysightDAQ970A
     *  assumes, only channels 21 and 22 can be used for performing current measurements.
     *
     *  \param channels The list of channels one wants to configure
     *   \param func The function that one wants to configure the channels to perform
     */
    // clang-format on
    void validateChannels(std::vector<unsigned> channels, Function func);

    //! \brief List of valid module channels for performing DC current
    //! measurements
    static const std::vector<unsigned> m_validChannelsDCI;

};  // class KeysightDAQ970A

#endif  // METER_KEYSIGHT_DAQ970A_H
