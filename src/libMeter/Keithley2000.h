#ifndef Keithley2000_H
#define Keithley2000_H
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "IMeter.h"

/*
 Keithley2000 multimeter
 Author: Jay Chan
 Date: Mar 2022
 Reference 1:
 https://download.tek.com/manual/2000-900_J-Aug2010_User.pdf
 2000-Scan card note:
 https://www.tek.com/default-accessory-series-manual/model-2000-scan-scanner-card
*/
class Keithley2000 : public IMeter {
 public:
    Keithley2000(const std::string& name);

    /** ping the device
     * @param dev: index of the device to ping (if there are multiple parts
     * connected) dev = 0 is for the main meter dev > 0 is for other parts
     * that's connected to the meter
     */
    virtual bool ping(unsigned dev = 0);

    virtual std::string identify();

    virtual void reset();

    /** measure DC voltage (unit: V)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     */
    virtual double measureDCV(unsigned channel = 0);
    virtual std::vector<double> measureDCV(
        const std::vector<unsigned>& channels);

    /** measure DC current (unit: A)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     */
    virtual double measureDCI(unsigned channel = 0);
    virtual std::vector<double> measureDCI(
        const std::vector<unsigned>& channels);

    /*
     measure resistance (unit: Ohm)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     * @param use4w: whether or not use 4-wire method
    */
    virtual double measureRES(unsigned channel = 0, bool use4w = false);
    virtual std::vector<double> measureRES(
        const std::vector<unsigned>& channels, bool use4w = false);

 private:
    void send(const std::string& cmd);

    /*
     Switch scanner card channel (provided the scanner card exists)
     Referemce:
     https://www.tek.com/default-accessory-series-manual/model-2000-scan-scanner-card
     * @param channel: channel ID of the scanner card to switch to
    */
    void sendScanCommand(std::string channel);

    std::string sendreceive(const std::string& cmd);
    void autowait();

    std::chrono::milliseconds m_wait{10};
};

#endif
