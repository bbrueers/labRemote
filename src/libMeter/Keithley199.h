#ifndef Keithley199_H
#define Keithley199_H
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "IMeter.h"

/*
 Keithley199 multimeter
 Author: Emily Thompson
 Date: Aug 2022
 Reference 1:
 https://download.tek.com/manual/199_901_01D.pdf
 The basic reading string that Keithley199 sends over the bus is in ASCII
 characters of the form: NDCV-1.23456E+0

 where: N indicates a normal reading (O would indicate an overflow),
        DCV shows the function in effect (in this case, DC volts),
        -1.23456 is the reading, and
        E+0 is the exponent.
*/

class Keithley199 : public IMeter {
 public:
    Keithley199(const std::string& name);

    /** ping the device
     * @param dev: index of the device to ping (if there are multiple parts
     * connected) dev = 0 is for the main meter dev > 0 is for other parts
     * that's connected to the meter
     */
    virtual bool ping(unsigned dev = 0);

    virtual std::string identify();

    virtual void reset();

    // Check for overflow in reading
    virtual std::string getOutputStatus(std::string output);

    // Check which reading was performed with meter
    virtual std::string getOutputType(std::string output);

    // Interpret string output from meter
    virtual double getOutputReading(std::string output);

    /** measure DC voltage (unit: V)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     */
    virtual double measureDCV(unsigned channel = 0);

    /** measure DC current (unit: A)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     */
    virtual double measureDCI(unsigned channel = 0);

    /*
     measure resistance (unit: Ohm)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
    */
    virtual double measureRES(unsigned channel = 0, bool use4w = false);

 private:
    void send(const std::string& cmd);

    std::string sendreceive(const std::string& cmd);
    void autowait();

    std::chrono::milliseconds m_wait{10};
};

#endif
