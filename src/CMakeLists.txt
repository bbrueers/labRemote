#
# Check for recent compiler
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    message(FATAL_ERROR "GCC version must be at least 4.8!")
  endif()
elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 3.2)
    message(FATAL_ERROR "Clang version must be at least 3.2!")
  endif()
else()
  message(WARNING "You are using an unsupported compiler! Compilation has only been tested with Clang and GCC.")
endif()

if(CMAKE_BUILD_TYPE MATCHES Debug)
  add_compile_options(-g)
else()
  add_compile_options(-O2)
endif()

#
# Check dependencies

# Find gclib (Galil motion controller library)
find_package( libgclib )

# Find libftdi1
find_package( libftdi1 )

# Find libmpsse
find_package( libmpsse )

# Find lusb-1.0
find_package( libusb-1.0 )

if ( (${LIBFTDI1_FOUND}) AND (${LIBMPSSE_FOUND}) )
  message(STATUS "Enabling FTDI and MPSSE support")
  set(ENABLE_FTDI 1)
  add_definitions(-DENABLE_FTDI)
else()
  message(STATUS "Disabling FTDI code due to missing libftdi1 or libmpsse:")
  message(STATUS "  LIBFTDI1_FOUND = ${LIBFTDI1_FOUND}")
  message(STATUS "  LIBMPSSE_FOUND = ${LIBMPSSE_FOUND}")
endif()

if (${LIBUSB_1_FOUND})
    set(ENABLE_USB 1)
    add_definitions(-DLIBUSB_ENABLED)
endif()

# Find libgpib
find_package ( libgpib )

if ( (${LIBGPIB_FOUND}) )
  set(ENABLE_GPIB 1)
else()
  message(STATUS "Disabling linux-GPIB code due to missing libgpib (LIBGPIB_FOUND = ${LIBGPIB_FOUND})")
endif()

#
# copy resource files to specified location(s) (as is, this needs to be done before the add_subdirectory calls)
#
set(LABREMOTE_SCHEMA_FILENAME "labremote_config_schema.json")
file(COPY schema/${LABREMOTE_SCHEMA_FILENAME} DESTINATION ${CMAKE_BINARY_DIR}/share/schema)
INSTALL(FILES schema/${LABREMOTE_SCHEMA_FILENAME} DESTINATION ${CMAKE_INSTALL_PREFIX}/share/labRemote/schema)

#NB:  - USE_PYTHON is enabled when python bindings are requested
#     - SKBUILD is enabled when building a python package
#       - SKBUILD uses '-DUSE_PYTHON=ON'

#
# Add libraries
add_subdirectory(exts)
add_subdirectory(libChiller)
add_subdirectory(libCom)
add_subdirectory(libDataSink)
add_subdirectory(libDevCom)
add_subdirectory(libEquipConf)
add_subdirectory(libPS)
add_subdirectory(libUtils)
add_subdirectory(libMeter)

# libs not needed/used for python bindings
if (NOT SKBUILD)
  add_subdirectory(libLoad)
  add_subdirectory(libScope)
endif()

#
# Add binaries
if (NOT SKBUILD)
  add_subdirectory(examples)
  add_subdirectory(tools)
endif()

#
# Add the python module subdirectory if flag is set and the
# dependencies (pybind) are found
if(USE_PYTHON AND PYBIND_OK)
    message(STATUS "Building labRemote python module")
    set(PP [=[\$$PYTHONPATH]=])
    add_custom_target(do_always ALL DEPENDS labRemote
                      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --cyan
                      "Python bindings for ${PYTHON_EXECUTABLE} were built, use: export PYTHONPATH=${PROJECT_BINARY_DIR}/lib:${PP}")
    add_subdirectory(labRemote)
endif()
